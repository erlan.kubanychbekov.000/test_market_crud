from rest_framework.generics import RetrieveUpdateAPIView
from users.models import Profile
from .serializers import ProfileSerializer
from rest_framework.permissions import IsAuthenticated


class UpdateProfileUserAPI(RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        current_user = self.request.user
        if current_user.is_authenticated:
            queryset = Profile.objects.filter(user=current_user)
            return queryset

    def get_object(self):
        return self.request.user.profile
