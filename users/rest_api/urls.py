from django.urls import path
from .views import UpdateProfileUserAPI

urlpatterns = [
    path('profile/', UpdateProfileUserAPI.as_view(), name='update-profile'),
]
