from django.views.generic import View
from users.models import Profile
from .forms import ProfileForm
from django.shortcuts import render, HttpResponse, redirect


class ProfileView(View):
    """Profile view."""
    model = Profile
    template_name = 'users/profile.html'
    form_class = ProfileForm
    context_object_name = 'profile'

    def get_profile(self, request):
        if request.user.is_authenticated:
            current_user_id = request.user.id
            # create one2one object
            profile, created = Profile.objects.get_or_create(user_id=current_user_id)
            return profile
        else:
            return None

    def get_context(self, request):
        profile = self.form_class(instance=self.get_profile(request))
        context = {
            self.context_object_name: profile
        }
        return context

    def get(self, request):
        context = self.get_context(request)
        if context[self.context_object_name] is not None:
            return render(request, self.template_name, context)
        else:
            return HttpResponse('Please login to view your profile.')

    def post(self, request):
        if request.user.is_authenticated:
            profile = self.form_class(instance=self.get_profile(request), data=request.POST, files=request.FILES)
            if profile.is_valid():
                profile.save()
                return redirect('users:profile')
            else:
                context = {
                    self.context_object_name: profile
                }
                return render(request, self.template_name, context)
        else:
            return HttpResponse('Please login to view your profile.')