from django.urls import path
from users.web.views import ProfileView

app_name = 'users'

urlpatterns = [
    path('profile/', ProfileView.as_view(), name='profile')
]
