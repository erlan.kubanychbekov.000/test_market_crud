## Тестовый проект по функционалу CRUD

#### Инструкция по запуску проекта:

Для запуска проекта требуется наличие Python, Docker и Docker Compose.

##### 1. Перейдите в корневую директорию проекта и создайте файл .env со следующими данными:
```Text
SECRET_KEY=django-insecure-16$2fwt$s1!=)uccq@_pj#q%$*#g%6&ier!d=w_zgs&7piad-3
DEBUG=1
POSTGRES_DB=market
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST=db
POSTGRES_POSTR=5432
REDIS_PORT=6379
```
Вы также можете скопировать и вставить эти данные в файл `.env`.


##### 2. Запустите контейнеры Docker:
```commandline
docker-compose up -d --build
```
После успешного запуска, ваш проект будет доступен по адресу http://127.0.0.1:8000/.


##### 3. Создайте администратора:
Зайдите в контейнер веб-приложения:
```commandline
docker-compose exec -it market_web_1 bash
```
Затем создайте администратора:
```commandline
python manage.py createsuperuser
```

##### 4. Доступ к Swagger API и схеме API:

После запуска проекта, вы можете получить доступ к документации Swagger API, схеме API и интерфейсу Swagger UI, перейдя по следующему адресу:

http://127.0.0.1:8000/schema/swagger-ui/

Этот URL позволяет вам изучить API вашего проекта и выполнить запросы к нему прямо из браузера.

Дополнительно вы можете внести изменения в этот файл README по мере необходимости, чтобы обеспечить пользователям более подробные инструкции по использованию вашего проекта.