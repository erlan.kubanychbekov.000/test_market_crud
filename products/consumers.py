import json
from channels.generic.websocket import AsyncWebsocketConsumer


class NotificationConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        await self.channel_layer.group_add('notifications', self.channel_name)

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard('notifications', self.channel_name)

    async def receive(self, text_data):
        data = json.loads(text_data)
        message_type = data['type']
        message = data['message']

        if message_type == 'send_notification':
            await self.send(text_data=json.dumps({
                'type': 'send_notification',
                'message': message,
            }))

    async def send_notification(self, event):
        message = event['message']

        # Send the message to the client
        await self.send(text_data=json.dumps({
            'message': message
        }))
