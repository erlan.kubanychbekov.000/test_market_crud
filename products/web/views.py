from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from products.models import Product, ProductCategory

from products.web.forms import ProductForm, CategoryForm


class ProductListView(ListView):
    """View for getting a list of products"""

    model = Product
    context_object_name = 'products'
    template_name = 'products/list.html'

    def get_queryset(self):
        # Query with joining categories and
        # getting only the name of the category for which you don't need to load databases.
        queryset = self.model.objects.select_related('category').values(
            'title',
            'price',
            'category__title',
            'category__pk',
            'pk')

        return queryset


class CreateProductView(CreateView):
    """View for creating a product."""

    model = Product
    template_name = 'products/add.html'
    form_class = ProductForm

    def get_success_url(self):
        return reverse_lazy('products:update', kwargs={'pk': self.object.pk})


class ProductUpdateView(UpdateView):
    """View for updating a product."""
    model = Product
    template_name = 'products/detail.html'
    form_class = ProductForm

    def get_success_url(self):
        return reverse_lazy('products:update', kwargs={'pk': self.object.pk})


class ProductDeleteView(DeleteView):
    """View for deleting a product."""
    model = Product
    success_url = reverse_lazy('products:list')


class CategoryUpdateView(UpdateView):
    """View for updating a product."""
    model = ProductCategory
    template_name = 'products/category_update.html'
    form_class = CategoryForm

    def get_success_url(self):
        return reverse_lazy('products:update', kwargs={'pk': self.object.pk})


class CategoryProductListView(ListView):
    """View for getting a list of category"""

    model = ProductCategory
    context_object_name = 'categories'
    template_name = 'products/category_list.html'

    def get_queryset(self):
        # Query with joining parent_category and
        # getting only the title, pk of the parent_category for which you don't need to load databases.
        queryset = self.model.objects.prefetch_related('parent_category').values('title', 'pk')

        return queryset


class CategoryProductDeleteView(DeleteView):
    """View for deleting a category."""
    model = Product
    success_url = reverse_lazy('products:category_list')


class CreateCategoryProductView(CreateView):
    """View for creating a product category."""

    model = ProductCategory
    template_name = 'products/add_category.html'
    form_class = CategoryForm

    def get_success_url(self):
        return reverse_lazy('products:category_list')
