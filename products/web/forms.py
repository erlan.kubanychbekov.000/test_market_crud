from django import forms
from products.models import Product, ProductCategory


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title', 'price', 'category']
        labels = {
            'title': 'Заголовок',
            'price': 'Цена',
            'category': 'Категория'
        }


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = ['title', 'parent_category']
        labels = {
            'title': 'Заголовок',
            'parent_category': 'Категория',

        }
