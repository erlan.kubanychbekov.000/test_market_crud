from django.urls import path
from .views import (
    ProductListView,
    CreateProductView,
    ProductUpdateView,
    ProductDeleteView,
    CategoryUpdateView,
    CategoryProductListView,
    CategoryProductDeleteView,
    CreateCategoryProductView
)

app_name = 'products'

urlpatterns = [
    path('', ProductListView.as_view(), name='list'),
    path('add/', CreateProductView.as_view(), name='add'),
    path('<int:pk>/', ProductUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', ProductDeleteView.as_view(), name='delete'),
    path('category/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('category/list/', CategoryProductListView.as_view(), name='category_list'),
    path('category/add/', CreateCategoryProductView.as_view(), name='category_add'),
    path('delete/category/<int:pk>/', CategoryProductDeleteView.as_view(), name='delete_category'),

]
