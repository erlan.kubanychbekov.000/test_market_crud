from django.db import models


class ProductCategory(models.Model):
    """Category class only for products"""
    title = models.CharField(verbose_name='Заголовок', max_length=50)
    parent_category = models.ManyToManyField('self', verbose_name='Родительская категория', null=True, blank=True)

    # data_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    """Class Product"""
    title = models.CharField(verbose_name='Заголовок', max_length=50)
    price = models.DecimalField(verbose_name='Цена', max_digits=10, decimal_places=2)
    category = models.ForeignKey(ProductCategory, verbose_name='Категория', on_delete=models.SET_NULL, null=True,
                                 blank=True)

    # data_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
