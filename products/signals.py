from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from .models import Product


@receiver(post_save, sender=Product)
def send_notification_on_product_creation(sender, instance, **kwargs):
    if kwargs.get('created', False):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'notifications',
            {
                'type': 'send_notification',
                'message': 'Новый продукт создан: {}'.format(instance.title)
            }
        )
