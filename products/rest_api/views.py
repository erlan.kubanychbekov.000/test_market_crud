from rest_framework.viewsets import ModelViewSet
from products.models import Product, ProductCategory
from .serializers import ProductSerializer, CategorySerializer


class ProductModelViewSet(ModelViewSet):
    queryset = Product.objects.select_related('category').all()
    serializer_class = ProductSerializer


class CategoryModelViewSet(ModelViewSet):
    queryset = ProductCategory.objects.prefetch_related('parent_category').all()
    serializer_class = CategorySerializer
