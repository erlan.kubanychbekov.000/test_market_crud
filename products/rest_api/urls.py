from rest_framework.routers import DefaultRouter
from .views import ProductModelViewSet, CategoryModelViewSet

router = DefaultRouter()
router.register(r'products', ProductModelViewSet)
router.register(r'category', CategoryModelViewSet)

urlpatterns = router.urls
